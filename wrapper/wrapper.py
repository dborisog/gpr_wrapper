import logging
import requests
import sys
import time
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler


class DztHandler(PatternMatchingEventHandler):
    patterns = ["*.py"]

    def process(self, event):
        """
        event.event_type 
            'modified' | 'created' | 'moved' | 'deleted'
        event.is_directory
            True | False
        event.src_path
            path/to/observed/file
        """
        # the file will be processed there
        # r = requests.put(
        #     'http://localhost:8888/api/position/', 
        #     json = {'position': line.decode("utf-8")})
        print(event.src_path, event.event_type)
        files = {'file': open(event.src_path,'rb')}
        with open(event.src_path, 'rb') as data:
            requests.put(
                'http://localhost:8888/api/gpr/',
                files=files)


    def on_created(self, event):
        self.process(event)


if __name__ == "__main__":
    args = sys.argv[1:]
    observer = Observer()
    observer.schedule(DztHandler(), path=args[0] if args else '.')
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
