# gpr_wrapper/placeholder/manage.py

from cheroot import wsgi
from project import create_app
import os


def get_env_variable(var_name):
    """Get environment variables."""
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise Exception(error_msg)

SITE_URL = get_env_variable('SITE_URL')
SITE_PORT = int(get_env_variable('SITE_PORT'))


app = create_app()

addr = SITE_URL, SITE_PORT
server = wsgi.Server(addr, app)

if __name__ == '__main__':
    try:
        server.start()
    except KeyboardInterrupt:
        server.stop()
