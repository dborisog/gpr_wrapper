# gpr_wrapper/placeholder/project/config.py

class BaseConfig:
    """Base configuration"""
    TESTING = False
    SECRET_KEY = 'my_precious'


class DevelopmentConfig(BaseConfig):
    """Development configuration"""
    SITE_URL = 'localhost'
    SITE_PORT = '8888'


class TestingConfig(BaseConfig):
    """Testing configuration"""
    TESTING = True


class ProductionConfig(BaseConfig):
    """Production configuration"""
    pass
