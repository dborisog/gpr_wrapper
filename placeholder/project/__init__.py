# gpr_wrapper/placeholder/project/__init__.py

from flask import Flask
import os


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    UPLOAD_FOLDER = 'data'
    UPLOAD_FOLDER = os.path.join(
        os.path.dirname(os.getcwd()),
        'data')
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

    # register blueprints
    from project.api.gpr import gpr_blueprint
    app.register_blueprint(gpr_blueprint)

    # shell context for flask cli
    app.shell_context_processor({'app': app})
    return app
