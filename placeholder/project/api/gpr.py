# gpr_wrapper/placholder/project/api/gpr.py

from datetime import datetime
from flask import Blueprint, jsonify, render_template, request
from flask import current_app as app
import os
import requests
from werkzeug.utils import secure_filename


gpr_blueprint = Blueprint('gpr', __name__)


ALLOWED_EXTENSIONS = set(['py'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@gpr_blueprint.route('/')
def home():
    """Return basic hello page."""
    print(request)
    return 'Basic wrapper for GPR processing.'


@gpr_blueprint.route('/api/gpr/', methods=['GET', 'PUT'])
def position():
    """Return basic hello page."""
    if request.method == 'PUT':

        if 'file' not in request.files:
            flash('No file part')
            return 'No file part'
        file = request.files['file']

        if file.filename == '':
            flash('No selected file')
            return 'No selected file'

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            print(os.stat(os.path.join(app.config['UPLOAD_FOLDER'], filename)))

            return 'Allowed file processed'

        return 'PUT processed'

    if request.method == 'GET':
        print(request)
        return 'GET processed'

