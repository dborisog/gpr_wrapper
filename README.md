

# Installation

## Clone the repository

https://bitbucket.org/dborisog/dpro_wrapper

## Install Python 3 and relevant packages

Install, or ensure that the following programs are installed, Python 3, pip, virtualenv according to the instructions in the [blogpost](http://timmyreilly.azurewebsites.net/python-pip-virtualenv-installation-on-windows/). You can check if each of the programs are installed by running ```python --version```, ```pip --version```, and ```virtualenv' --version``` respectively.

pip is a package manager for Python, while virtualenv allows to create virtual environment. The former simplifief package management, the latter allows to keep the main system clean and avoid package dependency conflicts in different projects.

## Create virtual environment

Open the work directory using Command Prompt. Create virtual environment by executing the following command ```virtualenv venv```

If ```python --version``` returns Python 2 and you don't want to change it, you create virtual environment by running ```virtualenv -p python3 venv```

Add ```call "config.bat"``` at the end of ```venv\Scripts\activate.bat```.

On Windows OS, you activate virtual environment by executing ```venv\Scripts\activate``` script, you should get '(venv)' prior the work directory if activated, e.g. ```(venv) D:\projects\iconst\gpr_wrapper>```. You deactivate virtual environment by executing ```deactivate``` command.

## Install python packages for the wrapper

Ensure that virtual environment is on. Execute ```pip install -r requirements.txt``` in the work directory. This would install all python packages for this wrapper. You can ensure that all relevant packages are installed by  manually comparing lists from requirements.txt and from the list produced by ```pip freeze```.


# The wrapper

## Assumptions

a single directory starting empty for the demonstration

## Structure

wrapper folder contains a script that monitors a directory for new files.

placeholder folder contains a placeholder for udetect microservice that can be used for development and testing purposes.

# Testing

Open a terminal window, activate the virtual environment, go to ```gpr_wrapper/placeholder/``` and execute ```python manage.py```.

Open another terminal windows, activate the virtual environment, go to ```gpr_wrapper/wrapper/``` and run ```python wrapper.py```. The wrapper.py generates PUT requests and sends these to 'placeholder'.

# Use

Open another terminal windows, activate the virtual environment, go to ```gpr_wrapper/wrapper/``` and run ```python wrapper.py```. The wrapper.py generates PUT requests and sends these to 'placeholder'.

# Review of approaches

* review
    * https://blog.philippklaus.de/2011/08/watching-directories-for-changes-using-python_-_an-overview 
    * http://timgolden.me.uk/python/win32_how_do_i/watch_directory_for_changes.html
* Linux
    * [inotify](https://github.com/dsoprea/PyInotify) - functionality is available from the Linux kernel and allows you to register one or more directories for watching, and to simply block and wait for notification events.
    * [pynotify example](https://www.saltycrane.com/blog/2010/04/monitoring-filesystem-python-and-pyinotify/), [example](https://linode.com/docs/development/monitor-filesystem-events-with-pyinotify/)
    * [cron]-based solution
    * [pure bash](https://www.jpablo128.com/how-to-detect-changes-in-a-directory-with-bash/), [example](https://www.raspberrypi.org/forums/viewtopic.php?t=21346)
    * [watcher](https://github.com/gregghz/Watcher) a daemon that watches specified files/folders for changes and fires commands in response to those changes; [docs example](http://kushellig.de/linux-file-auto-sync-directories/)
* Windows 
    * [FileSystemWatcher](https://msdn.microsoft.com/en-us/library/system.io.filesystemwatcher(VS.80).aspx), and an [implementation](http://www.ironpython.info/index.php?title=Watching_the_FileSystem)
    * [FindFirstChangeNotification API]
    * [ReadDirectoryChanges API](https://qualapps.blogspot.com/2010/05/understanding-readdirectorychangesw.html)
    * [custom](http://discourse.techart.online/t/watching-directories-with-python/206/5)
    * [custom](http://code.activestate.com/recipes/156178-watching-a-directory-under-win32/)
* cross-platform
    * [os.listdir]
    * [gevent](https://github.com/gevent/gevent) 
    * [glances](https://github.com/nicolargo/glances)
    * [QFileSystemWatcher](http://pyqt.sourceforge.net/Docs/PyQt4/qfilesystemwatcher.html), an [example](http://blog.mathieu-leplatre.info/filesystem-watch-with-pyqt4.html)
    * [watchdog](https://github.com/gorakhargosh/watchdog), [docs](http://pythonhosted.org/watchdog/), [example](http://www.funphp.com/2017/03/29/watching-a-directory-for-files-event-in-python/), [example](http://brunorocha.org/python/watching-a-directory-for-file-changes-with-python.html) an example of [using watchdog](http://brunorocha.org/python/watching-a-directory-for-file-changes-with-python.html)

It seems [watchdog](https://github.com/gorakhargosh/watchdog) is the most promising out of the options.